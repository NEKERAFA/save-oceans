import "graphics" for Canvas

import "./src/gamestate" for GameState
import "./src/scenes/mainmenu" for MainMenu

class Game {
    static init() {
        Canvas.resize(160, 120)
        GameState.push(MainMenu.new())
    } 

    static update() {
        GameState.update()
    }

    static draw(dt) {
        GameState.draw()
    }
}
