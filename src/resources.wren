import "graphics" for ImageData
import "audio" for AudioEngine

/**
 * This class represents a resources manager
 * It contains functions to load all resources that the game needs
 *
 * Note: Another function of a resource manager is to cached the resources that
 * have already been loaded to reduce disk drive access, but Dome do this
 * function by default
 */
class Resources {
    // The folder where assets must be
    static Assets_Path { "assets" }

    // The asset folders
    static Images_Path { "%(Resources.Assets_Path)/images" }
    static Sounds_Path { "%(Resources.Assets_Path)/sounds" }

    // Loads a image or, if it have already been loaded, return it from cache
    static loadImage(name) {
        var path = "%(Resources.Images_Path)/%(name).png"

        if (ImageData[path] == null) {
            var image = ImageData.loadFromFile(path)
            return image
        }

        return ImageData[path]
    }

    // Loads a sound or, if it have already been loaded, return it from cache
    static loadSound(name) {
        return AudioEngine.load(name, "%(Resources.Sounds_Path)/%(name).ogg")
    }
}
