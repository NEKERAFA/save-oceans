/**
 * Represents game settings
 */

class Settings {
    static Volume {
        if (!__volume) __volume = 9
        return __volume
    }

    static Volume=(value) {
        __volume = value
    }

    static Button {
        if (!__button) __button = "left"
        return __button
    }

    static Button=(value) {
        __button = value
    }
}
