import "math" for Vector
import "input" for Mouse
import "graphics" for Canvas, Color
import "audio" for AudioEngine

import "./src/gamestate" for GameState
import "./src/resources" for Resources
import "./src/collisions" for Rectangle, Collisions
import "./src/scene" for Scene
import "./src/random" for Random
import "./src/settings" for Settings

/**
 * Represents the game scene
 */

class GameScene is Scene {
    radarAnim { 0.5 }
    radarCooldown { 4 }

    construct new(newGame, level) {
        _newGame = newGame

        _fadeIn = newGame

        _fadeOut = {
            "show": true,
            "dt": 0,
            "alpha": 255
        }

        _level = level

        _radar = {
            "show": level > 0,
            "anim": {
                "show": level == 0,
                "dt": 0
            },
            "cooldown": {
                "show": false,
                "dt": 0
           }
        }

        _clock = {
            "show": level > 1,
            "time": (60 - (level - 2).max(0)).min(60).max(10)
        }
    }

    enter() {
        populateCells()

        _point = {
            "image": Resources.loadImage("point"),
            "show": false
        }

        _ship = {
            "image": Resources.loadImage("ship"),
        }

        if (_newGame)  {
            _ship["move"] = true
            _ship["y"] = 120
        }

        if (_level == 0) {
            getRadarPoints()
        }

        _seaColor = Color.rgb(0, 150, 255)
        _fogColor = Color.rgb(255, 255, 255, _newGame ? 0 : 192)

        _radar["image"] = Resources.loadImage("radar")
        _back = Resources.loadImage("back")

        _info = Resources.loadImage("info")
        _plastic = Resources.loadImage("plastic_icon")
        _bottle = Resources.loadImage("bottle_icon")
        _mask = Resources.loadImage("mask_icon")

        _plastics = Resources.loadImage("plastics")
        _masks = Resources.loadImage("masks")
        _bottles = Resources.loadImage("bottles")

        _check = Resources.loadImage("check")
        _wrong = Resources.loadImage("wrong")
    }

    populateCells() {
        _cells = {}
        var positions = []

        for (column in 0..5) {
            _cells[column] = {}

            for (row in 0..3) {
                 positions.add({ "column": column, "row": row })

                 _cells[column][row] = {
                     "show": true,
                     "item": "water"
                 }
            }
        }

        var rand = Random.new()
        var items = ["plastics", "bottles", "masks"] 
        _selectedItems = {}

        var max_items = rand.int(1,3)

        for (i in 0..max_items) {
             var item = rand.sample(items)

             _selectedItems[item] = _selectedItems[item] ? _selectedItems[item] + 1 : 1

             var pos = rand.sample(positions)
             
             _cells[pos["column"]][pos["row"]]["item"] = item
        }
    }

    change(level) {}

    leave() {}

    update() {
        updateCells()

        if (!_fadeIn && !_fadeOut["show"]) {
            if (_clock["time"].floor > 0) {
                checkMousePosition()
            }
            
            if (_radar["anim"]["show"]) {
                updateRadarAnim()
            }

            if (_radar["show"]) {
                checkRadar()
            }

            if (_clock["show"]) {
                updateClock()
            }

            checkEndCondition()
        }

        if (_newGame) {
	    checkFadeIn()
        }

        if (_fadeOut["show"]) {
            checkFadeOut()
        }

        checkExit()
    }

    updateClock() {
        _clock["time"] = (_clock["time"] - 0.016).max(0)
    }

    checkMousePosition() {
        var fogRect = Rectangle.new(Vector.new(8, 4), 144, 96)
        _point["show"] = Collisions.checkRectangle(Mouse.pos, fogRect)

        if (_point["show"]) {
            _point["column"] = ((Mouse.x - 8) / 24).floor.clamp(0, 5)
            _point["row"] = ((Mouse.y - 4) / 24).floor.clamp(0, 3)

            if (Mouse[Settings.Button].justPressed && _cells.containsKey(_point["column"]) && _cells[_point["column"]].containsKey(_point["row"])) {
                _cells[_point["column"]][_point["row"]]["show"] = false
                _cells[_point["column"]][_point["row"]]["dt"] = 0

                var splash = AudioEngine.play("watersplash")
                splash.volume = Settings.Volume / 10 / 4

                if (_clock["show"] && _cells[_point["column"]][_point["row"]]["item"] == "water") {
                    _clock["time"] = _clock["time"].floor - 10
                }
            }
        }
    }

    updateCells() {
        _deletedCells = []

        for (column in _cells.keys) {
            for (row in _cells[column].keys) {
                if (!_cells[column][row]["show"]) {
                    _cells[column][row]["dt"] = _cells[column][row]["dt"] + 0.016

                    if (_cells[column][row]["dt"] >= 1) {
                        _deletedCells.add({ "row": row, "column": column })
                    }
                }
            }
        }

        for (cell in _deletedCells) {
            _cells[cell["column"]].remove(cell["row"])
            if (_cells[cell["column"]].count == 0) {
                _cells.remove(cell["column"])
            }
        }
    }

    checkExit() {
        var backRect = Rectangle.new(Vector.new(5, 103), 11, 14)
        if (Collisions.checkRectangle(Mouse.pos, backRect) && Mouse[Settings.Button].justPressed) {
            GameState.pop(_level.min(2))
        }
    }

    checkFadeIn() {
        if (_fogColor.a < 192) {
            _fogColor.a = (_fogColor.a + 10).min(192)
        }

        if (_ship["move"]) {
            _ship["y"] = (_ship["y"] - 1).max(96)
            if (_ship["y"] == 96) {
                _ship["move"] = false
            }
        }

        if (_fogColor.a == 192 && _ship["y"] == 96) {
            _fadeIn = false
        }
    }

    checkFadeOut() {
        if (_fadeOut["dt"] < 2) {
            _fadeOut["dt"] = (_fadeOut["dt"] + 0.016).min(3)
        } else {
            _fadeOut["alpha"] = (_fadeOut["alpha"] - 10).max(0)

            if (_fadeOut["alpha"] == 0) {
                _fadeOut["show"] = false
            }
        }
    }

    checkEndCondition() {
        var anyItem = false

        for (column in _cells.keys) {
            for (row in _cells[column].keys) {
                if (_cells[column][row]["item"] != "water") {
                    anyItem = true
                    break
                }
            }

            if (anyItem) {
                break
            }
        }

        if (!anyItem) {
            var nextLevel = GameScene.new(false, _level + 1)
            GameState.change(nextLevel)
        }

        if (_clock["show"] && _clock["time"].floor == 0) {
            var overRect = Rectangle.new(Vector.new(22, 49), 116, 22)
            if (Collisions.checkRectangle(Mouse.pos, overRect) && Mouse[Settings.Button].justPressed) {
                GameState.pop(2)
            }
        }
    }

    checkRadar() {
        if (_radar["cooldown"]["show"]) {
            updateRadarCooldown()
        }

        var radarRect = Rectangle.new(Vector.new(113, 105), 14, 10)
        if (Collisions.checkRectangle(Mouse.pos, radarRect) && Mouse[Settings.Button].justPressed && !_radar["cooldown"]["show"]) {
            getRadarPoints()
            _radar["anim"]["show"] = true
            _radar["cooldown"]["show"] = true
        }
    }

    getRadarPoints() {
        var points = []

        var rand = Random.new()        

        for (column in _cells.keys) {
            for (row in _cells[column].keys) {
                if (_cells[column][row]["item"] != "water") {
                    points.add({
                        "x": 8 + column * 24 + rand.int(1) * 24,
                        "y": 4 + row * 24 + rand.int(1) * 24
                    })
                }
            }
        }

        _radar["points"] = points
    }

    updateRadarAnim() {
        _radar["anim"]["dt"] = _radar["anim"]["dt"] + 0.016
    
        if (_radar["anim"]["dt"] >= radarAnim) {
            _radar["anim"]["dt"] = 0
            _radar["anim"]["show"] = false
        } 
    }

    updateRadarCooldown() {
        _radar["cooldown"]["dt"] = _radar["cooldown"]["dt"] + 0.016

        if (_radar["cooldown"]["dt"] >= radarCooldown) {
            _radar["cooldown"]["dt"] = 0
            _radar["cooldown"]["show"] = false
        }
    }

    draw() {
        Canvas.cls(_seaColor)

        drawFog()
        
        if (!_fadeIn && !_fadeOut["show"]) {
            if (_radar["anim"]["show"]) {
                drawRadarAnim()
            }
        
            if (_point["show"] && _clock["time"].floor > 0) {
                drawPoint()
            }
        }

        if (_fadeOut["show"]) {
            drawInfo()
        }

        drawShip()

        _back.draw(2, 102)

        if (_radar["show"]) {
            drawRadar()
        }

        if (_clock["show"]) {
            Canvas.print(_clock["time"].floor, 138, 106, Color.white)
        }

        if (_clock["time"].floor == 0) {
            _info.draw(22, 49)
            Canvas.print("Game Over", 46, 56, Color.black)
        }
    }

    drawFog() {
        for (column in _cells.keys) {
            for (row in _cells[column].keys) {
                
                if (_cells[column][row]["show"]) {
                    Canvas.rectfill(8 + column * 24, 4 + row * 24, 24, 24, _fogColor)
                } else {
                    if (_cells[column][row]["item"] == "bottles") {
                        _bottles.draw(8 + column * 24, 4 + row * 24)
                    } else if (_cells[column][row]["item"] == "masks") {
                        _masks.draw(8 + column * 24, 4 + row * 24)
                    } else if (_cells[column][row]["item"] == "plastics") {
                        _plastics.draw(8 + column * 24, 4 + row * 24)
                    }

                    var icon = _check
                    if (_cells[column][row]["item"] == "water") {
                        icon = _wrong
                    }

                    icon.transform({
                        "opacity": 1 - _cells[column][row]["dt"]
                    }).draw(8 + column * 24, 4 + row * 24)
                }
            }
        }
    }

    drawRadarAnim() {
        var animRatio = _radar["anim"]["dt"] / radarAnim

        var size = animRatio * 48
        var color = Color.rgb(Color.red.r, Color.red.g, Color.red.b, 255 - animRatio * 255)

        for (point in _radar["points"]) {
            Canvas.ellipse(point["x"] - size / 2, point["y"] - size / 2, point["x"] + size / 2, point["y"] + size / 2, color)
        }
    }

    drawRadar() {
        if (_radar["show"]) {
            _radar["image"].draw(112, 102)

            if (_radar["cooldown"]["show"]) {
                var cooldownRatio = _radar["cooldown"]["dt"] / radarCooldown

                Canvas.clip(112, 102 + cooldownRatio * 16, 16, 16 - cooldownRatio * 16)
                _radar["image"].transform({
                    "tint": Color.rgb(0, 0, 0, 192)
                }).draw(112, 102)
                Canvas.clip()
            }
        }
    }

    drawPoint() {
        _point["image"].draw(8 + _point["column"] * 24, 4 + _point["row"] * 24)
    }

    drawShip() {
        _ship["image"].draw(56, _ship["move"] ? _ship["y"] : 96)
    }

    drawInfo() {
        var black = Color.rgb(0, 0, 0, _fadeOut["alpha"])
        _info.transform({ "opacity": _fadeOut["alpha"] / 255 }).draw(22, 8)
        
        var offset = 0
        if (_selectedItems["plastics"]) {
            _plastic.transform({ "opacity": _fadeOut["alpha"] / 255 }).draw(27, 14)
            Canvas.print(_selectedItems["plastics"], 38, 15, black)
            offset = 35
        }

        if (_selectedItems["bottles"]) {
            _bottle.transform({ "opacity": _fadeOut["alpha"] / 255 }).draw(27 + offset, 14)
            Canvas.print(_selectedItems["bottles"], 38 + offset, 15, black)
            offset = offset + 35
        }

        if (_selectedItems["masks"]) {
            _mask.transform({ "opacity": _fadeOut["alpha"] / 255 }).draw(27 + offset, 14)
            Canvas.print(_selectedItems["masks"], 38 + offset, 15, black)
        }
    }
}
