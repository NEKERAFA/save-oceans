import "graphics" for Canvas, Color
import "audio" for AudioEngine
import "math" for Vector
import "input" for Mouse

import "./src/gamestate" for GameState
import "./src/resources" for Resources
import "./src/settings" for Settings
import "./src/random" for Random
import "./src/collisions" for Rectangle, Collisions
import "./src/scene" for Scene
import "./src/scenes/gamescene" for GameScene

/**
 * Represents the main menu scene
 */

class MainMenu is Scene {
    static FadeIn { 2 }
    static Clouds { 12 }

    construct new() {
        _level = 0

        Resources.loadSound("watersplash")
    }

    enter() {
        if (!__bgm) {
            Resources.loadSound("bitwaves")
        }

        _bgi = Resources.loadImage("background")
        _title = Resources.loadImage("title")

        var rand = Random.new()
        var bottom = rand.bool()

        _fadeIn = {
            "show": true,
            "dt": 0
        }

	_bottle = {
            "image": Resources.loadImage("bottle"),
            "dt": 0,
            "change": bottom ? -1 : 1,
            "wave": {
                "dt": 0,
                "show": bottom
            }
        }

        _clouds = {
            "image": Resources.loadImage("clouds"),
            "dt": 0
        }

        _settings = {
            "icon": Resources.loadImage("settings"),
            "back": Resources.loadImage("back"),
            "show": false,
            "dt": 0
        }
    }

    change(level) {
        _level = level
    }

    leave() {}

    update() {
        if (_fadeIn["dt"] > (MainMenu.FadeIn / 2)) {
            updateBg()
        }

        if (_fadeIn["show"]) {
            updateFadeIn()
        } else {
            if (_settings["show"]) {
                updateSettings()
            } else {
                updateMainScreen()
            }
        }
    }

    updateFadeIn() {
        _fadeIn["dt"] = _fadeIn["dt"] + 0.016

        if (_fadeIn["dt"] >= MainMenu.FadeIn) {
            _fadeIn["show"] = false
            __bgm = AudioEngine.play("bitwaves")
            __bgm.volume = Settings.Volume / 5
            __bgm.loop = true
        }
    }

    updateSettings() {
        _settings["dt"] = (_settings["dt"] + 0.016).min(1)

        if (_settings["dt"] == 1) {
            var backRect = Rectangle.new(Vector.new(5, 103), 11, 14)
            if (Collisions.checkRectangle(Mouse.pos, backRect) && Mouse[Settings.Button].justPressed) {
                _settings["show"] = false
                _settings["dt"] = 0
            }
        }

        var leftRect = Rectangle.new(Vector.new(150, 10), 8, 8)
        if (Collisions.checkRectangle(Mouse.pos, leftRect) && Mouse[Settings.Button].justPressed) {
            Settings.Volume = (Settings.Volume + 1).min(10)
            __bgm.volume = Settings.Volume / 10
        }

        var rightRect = Rectangle.new(Vector.new(Settings.Volume < 10 ? 118 : 110, 10), 8, 8)
        if (Collisions.checkRectangle(Mouse.pos, rightRect) && Mouse[Settings.Button].justPressed) {
            Settings.Volume = (Settings.Volume - 1).max(0)
            __bgm.volume = Settings.Volume / 10
        }

        var mouseRect = Rectangle.new(Vector.new(10, 20), 148, 8)
        if (Collisions.checkRectangle(Mouse.pos, mouseRect) && Mouse[Settings.Button].justPressed) {
            Settings.Button = Settings.Button == "left" ? "right" : "left"
        }
    }

    updateMainScreen() {
        var settingsRect = Rectangle.new(Vector.new(2, 102), 16, 16)
        if (Collisions.checkRectangle(Mouse.pos, settingsRect) && Mouse[Settings.Button].justPressed) {
            _settings["show"] = true
        }

        var startRect = Rectangle.new(Vector.new(50, 102), 62, 8)
        if (Collisions.checkRectangle(Mouse.pos, startRect) && Mouse[Settings.Button].justPressed) {
            GameState.push(GameScene.new(true, _level))
        }
    }

    updateBg() {
        _clouds["dt"] = (_clouds["dt"] + 0.016).min(MainMenu.Clouds)
        if (_clouds["dt"] == MainMenu.Clouds) {
            _clouds["dt"] = 0
        }

        _bottle["dt"] = (_bottle["dt"] + _bottle["change"] * 0.016).min(1.5).max(-1.5)
        if (_bottle["dt"] == 1.5 || _bottle["dt"] == -1.5) {
            _bottle["change"] = - _bottle["change"]
        }

        if (_bottle["wave"]["show"]) {
            _bottle["wave"]["dt"] = _bottle["wave"]["dt"] + 0.016

            if (_bottle["wave"]["dt"] >= 1) {
                _bottle["wave"]["dt"] = 0
                _bottle["wave"]["show"] = false
            }
        }   

        if (_bottle["dt"] == 1.5 || _bottle["dt"] == -1.5) {
            _bottle["wave"]["show"] = true
        }
    }

    draw() {
        drawBg()

        if (_fadeIn && _fadeIn["show"]) {
            drawFadeIn()
        } else if (_settings["show"]) {
            drawSettings()
        } else {
            drawMainScreen()
        }
    }

    drawBg() {
        _bgi.draw(0, 0)

        var cloudsRatio = _clouds["dt"] / MainMenu.Clouds
        _clouds["image"].draw(0 - cloudsRatio * 320, 0)
        _clouds["image"].draw(320 - cloudsRatio * 320, 0)

        if (_bottle["wave"]["show"]) {
            var waveRatio = _bottle["wave"]["dt"] / 1
            var waveColor = Color.rgb(255, 255, 255, 255 - waveRatio * 255)
            Canvas.ellipse(119 - 32 * waveRatio, 87 - 16 * waveRatio, 119 + 32 * waveRatio, 87 + 16 * waveRatio, waveColor)
        }

        _bottle["image"].draw(107, 69 + _bottle["dt"].floor)
    }

    drawFadeIn() {
        var animRatio = _fadeIn["dt"] / MainMenu.FadeIn

        Canvas.rectfill(0, 0, Canvas.width, Canvas.height, Color.rgb(0, 0, 0, 255 - 255 * (animRatio - 0.5).max(0) * 2))
        _title.draw(19, -27 + 57 * animRatio)
    }

    drawMainScreen() {
        _title.draw(19, 30)
        _settings["icon"].draw(2, 102)
        Canvas.print("New game", 50, 102, Color.white)
    }

    drawSettings() {
        var settingsRatio = _settings["dt"] / 1

        Canvas.rectfill(0, 0, Canvas.width, Canvas.height, Color.rgb(0, 0, 0, settingsRatio * 128))
        
        if (_settings["dt"] == 1) {
            Canvas.print("Volume", 10, 10, Color.white)
            Canvas.print("< %(Settings.Volume) >", Settings.Volume < 10 ? 118 : 110, 10, Color.white)            

            Canvas.print("Mouse button", 10, 20, Color.white)
            Canvas.print("%(Settings.Button == "left" ? "Left" : "Right")", Settings.Button == "left" ? 126 : 118, 20, Color.white)
        }

        _settings["back"].draw(2, 102)
    }
}
