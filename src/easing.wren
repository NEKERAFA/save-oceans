/**
 * Represents easing functions
 */

class Easing {
    static outBack(t, b, c, d) {
        return outBack(t, b, c, d, 1.70158)
    }

    static outBack(t, b, c, d, s) {
        System.print("%(t), %(b), %(c), %(d), %(s)")
        t = t / d - 1 
        return c * (t * t * ((s + 1) * t + s) + 1) + b
    }
}
