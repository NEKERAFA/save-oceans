/**
 * Represents a Scene in the game
 */

class Scene {
    change(level) { System.print("Implements change method") }

    enter() { System.print("Implements enter method") }

    leave() { System.print("Implements leave method") }

    update() { System.print("Implements update method") }

    draw() { System.print("Implements draw method") }
}
