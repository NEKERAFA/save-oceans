import "dome" for Process

/**
 * This class represents a game state manager
 * It contains the methods to pop, push and change scenes, and update and draw
 * its contains
 */

class GameState {
    static scenes {
        if (__scenes == null) {
            __scenes = [] // This is a stack of scenes
        }

        return __scenes
    }

    // Updates the last scene
    static update() {
        if (scenes.count > 0) {
            scenes[-1].update()
        }
    }

    // Draws the last scene
    static draw() {
        if (scenes.count > 0) {
            scenes[-1].draw()
        }
    }

    // Removes the last scene at the stack
    static pop(level) {
        if (scenes.count > 0) {
            scenes[-1].leave()
            scenes.removeAt(-1)
        }

        if (scenes.count == 0) {
            Process.exit()
        } else {
            scenes[-1].change(level)
        }
    }

    // Add new scene to the stack
    static push(scene) {
        if (scenes.count > 0) {
            scenes[-1].leave()
        }

        scenes.insert(-1, scene)
        scene.enter()
    }

    /**
     * Removes the last scene and add add another
     *
     * Note: If there is no scenes added, this method is like push
     */
    static change(scene) {
        if (scenes.count > 0) {
            scenes[-1].leave()
            scenes[-1] = scene
        } else {
            scenes.insert(-1, scene)
        }

        scene.enter()
    }
}
