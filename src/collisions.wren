/**
 * Implements all collision tests
 */

class Rectangle {
    pos { _position }
    position { _position }
    x { _position.x }
    y { _position.y }
    width { _width }
    height { _height }

    construct new(position, width, height) {
        _position = position
        _width = width
        _height = height
    }
}

class Collisions {
    static checkRectangle(pos, rect) {
        return (pos.x >= rect.x &&
                pos.x <= rect.x + rect.width &&
                pos.y >= rect.y &&
                pos.y <= rect.y + rect.height)
    }
}
