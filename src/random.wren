import "dome" for Platform

/**
 * A random generator number
 */

class Random {
    construct new() {
        _seed = Platform.time % (2.pow(15) - 1)
    }


    next() {
        var newSeed = (_seed + System.clock).floor % (2.pow(15) - 1)
        _seed = (newSeed * 1103515245 + 12345) % (2.pow(15) - 1)
        return _seed
    }

    float() {
        var number = next()
        return _seed / (2.pow(15) - 1)
    }

    float(end) {
        var number = float()
        return number * end
    }

    float(start, end) {
        var number = float()
        return start + (number * (end - start))
    }

    int(end) {
        var number = next()
        return number % (end + 1)
    }

    int(start, end) {
        var number = next()
        return start + (number % (end - start + 1))
    }

    bool() {
        var number = next()
        return number % 2
    }

    sample(list) {
        var pos = int(list.count - 1)
        return list[pos]
    }


    sample(list, count) {
        var selected = []

        for (i in 0...count) {
            selected.add(sample(list))
        }
        
        return selected
    }

    shuffle(list) {
        var tmp = list.toList

        for (i in 0...list.count) {
            var pos = int(i + 1)
            tmp.swap(j, i)
        }

        return tmp
    }   
}
